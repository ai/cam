#!/usr/bin/python

from setuptools import setup, find_packages

setup(
  name="cam",
  version="2.1",
  description="Minimal X509 Certification Authority management",
  author="ale",
  author_email="ale@incal.net",
  url="https://git.autistici.org/ai/cam",
  install_requires=[],
  setup_requires=[],
  zip_safe=False,
  packages=find_packages(),
  package_data={
    "cam": ["templates/*"]},
  entry_points={
    "console_scripts": [
      "cam = cam.main:main",
    ],
  },
)

