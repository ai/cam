
=====================================================
cam - minimal X509 Certification Authority management
=====================================================

``cam`` is a tiny Python program that can be used to manage a X509
Certification Authority for a small organization. It can only create
server certificates, so it is not going to be useful to manage an
X509-based client authentication infrastructure.

The intended usage involves describing the list of certificates to
generate in a configuration file, and using the ``cam`` tool to create
and renew them.


Installation
------------

``cam`` has few requirements:

* A moderately recent version of Python 2;
* OpenSSL (>=1.0.0) - specifically, the ``openssl`` binary.

Once you have downloaded the source code, system-wide installation is
simply a matter of::

    $ sudo python setup.py install


Configuration
-------------

The configuration file uses a standard INI-like syntax, consisting of
a number of sections. There are two special sections: ``ca`` and
``global``, any other section is interpreted as a certificate
definition.

The ``ca`` section contains the attributes of the CA itself, see the
example configuration file to see which attributes are supported.

The ``global`` section contains configuration parameters for ``cam``.
The only configuration parameter supported is ``root_dir``, which is
where all the CA private data will be stored.

Certificates are identified by a *tag* (the section name), so for
example given the following configuration snippet::

    [web]
    cn = www.domain.org

you would use the following command to generate it::

    $ cam --config=my.config gen web


Global
++++++

The ``global`` section contains options that affect the behavior of
the ``cam`` tool itself. You can usually leave this out altogether.

Available options:

``root_dir``
  This is where the CA private data will be stored. If you leave this
  parameter empty, or if you don't define a ``global`` section at all,
  this will default to the directory containing the configuration file.


Certification Authority
+++++++++++++++++++++++

The ``ca`` section specifies parameters for the Certification
Authority. Some of these are mandatory as they uniquely identify each
CA.

The following parameters specify options of the CA certificate itself.
They are only used once, at CA initialization time (when running ``cam
init``). Subsequent changes to these options will be ignored.

``cn``
  Value of the Common Name (CN) field in the X509 Subject.

``org``
  Value of the Organization (O) field in the X509 Subject.

``country``
  Value of the Country (C) field in the X509 Subject.

``email``
  Contact email, added to the X509 CA certificate.

``days``
  Number of days that the CA certificate will be valid for (default:
  3650, or 10 years).

``crl_url``
  Public URL where the CA Certificate Revocation List will be
  accessible (optional).

Other options:

``default_days``
  Number of days that a new certificate will be valid for (default: 365).

``bits``
  Size of the RSA key for the CA certificate, and also default key
  size for all newly created certificates (default: 2048).

``signature_algorithm``
  Digest algorithm to use for CA signatures (default: sha256).


Certificates
++++++++++++

Every other section defines options for a certificate. Some of these
options can be left unset, in which case a default value will be
provided by the ``ca`` section. ``cn`` must be always specified.

The following options are available:

``days``
  Number of days that this certificate will be valid for. If unset,
  will use ``ca.default_days``.

``cn``
  Common Name (CN) for the X509 Subject.

``ou``
  Organizational Unit (OU) for the X509 Subject (optional).

``alt_names``
  Space-separated list of alternate names for this certificate. These
  will be encoded as DNS entries in the certificate's X509v3 Subject
  Alternative Name field.


Usage
-----

Once you have created a configuration file, initialize the CA by
running::

    $ cam --config=my.config init

This will create the CA certificate and private key, and it will ask
you to set a passphrase for the key. Pick something secure.

Once this is done, you will be able to generate the certificates
described in the configuration using the ``cam gen`` command. For
example, if the configuration defines a certificate with a tag of
``web``::

    $ cam --config=my.config gen web

The tool will ask you for the CA passphrase, and it will create a
certificate and a private key in the CA private data directory. You
can obtain their path with::

    $ cam --config=my.config files web
    /your/ca/dir/public/certs/web.pem
    /your/ca/dir/private/web.key

At any time you can inspect the status of the configured certificates
(and see which ones are about to expire) using the ``cam list``
command::

    $ cam --config=my.config list


Standalone Install
------------------

The CA private keys are very sensitive information, so you'll want to
store them in some encrypted removable storage. You can bundle the
``cam`` application itself with the CA data by using ``virtualenv``::

    $ virtualenv --no-site-packages /secure/cam
    $ virtualenv --relocatable /secure/cam
    $ (cd /tmp ; git clone https://git.autistici.org/ai/cam.git \
       && /secure/cam/bin/python setup.py install)

Then you can simply mount your encrypted image wherever there is a
Python interpreter available (well, with the same architecture/OS too)
and run::

    $ /secure/cam/bin/cam --config=/secure/ca/my.config ...


