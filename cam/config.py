try:
    import ConfigParser as configparser
except ImportError:
    # python 3 has ConfigParser renamed
    import configparser
import os
from cam import cert
from cam import ca


class ConfigError(Exception):
    pass


def read_config(filename, password=None):
    parser = configparser.ConfigParser()
    if not parser.read(filename):
        raise ConfigError('File not found: %s' % filename)
    root_dir = os.path.dirname(os.path.abspath(filename))
    global_config = {}
    if parser.has_section('global'):
        global_config = dict(parser.items('global'))
        root_dir = global_config.get('root_dir', root_dir)
    ca_obj = ca.CA(root_dir, dict(parser.items('ca')), password=password)
    certs = []
    for section in parser.sections():
        if section in ('ca', 'global'):
            continue
        certs.append(cert.Cert(ca_obj, section, dict(parser.items(section))))
    return global_config, ca_obj, certs
