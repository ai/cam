import os


_template_dir = os.path.join(os.path.dirname(__file__), 'templates')


def render(output_file, template_name, args):
    tpl = open(os.path.join(_template_dir, template_name), 'r')
    outfd = open(output_file, 'w')
    outfd.write(tpl.read() % args)
    tpl.close()
    outfd.close()


def parse_bool(s):
    return s.lower() in ('1', 'y', 'yes', 'true', 'on')
