import mox
import os
import tempfile
import unittest
from cam import config


class ConfigTest(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.mox = mox.Mox()

    def tearDown(self):
        self.mox.VerifyAll()
        self.mox.UnsetStubs()
        os.system("rm -fr '%s'" % self.tmpdir)

    def test_read_config(self):
        test_cfg = '''
[global]
root_dir = root

[ca]
something = else

[cert1]
cn = test.com
'''
        cf_file = os.path.join(self.tmpdir, 'config')
        cf_fd = open(cf_file, 'w')
        cf_fd.write(test_cfg)
        cf_fd.close()
        self.mox.StubOutWithMock(config.ca, 'CA', use_mock_anything=True)
        config.ca.CA('root', {'something': 'else'}, password=None).AndReturn('ca')
        self.mox.StubOutWithMock(config.cert, 'Cert', use_mock_anything=True)
        config.cert.Cert('ca', 'cert1', {'cn': 'test.com'}).AndReturn('cert1')
        self.mox.ReplayAll()

        global_config, ca_obj, certs = config.read_config(cf_file)
        self.assertEqual('ca', ca_obj)
        self.assertEqual(['cert1'], certs)

    def test_read_config_nonexist(self):
        def f():
            config.read_config('nonexist.conf')
        self.assertRaises(config.ConfigError, f)


if __name__ == '__main__':
    unittest.main()
