import logging
import os
import tempfile
import shutil
import subprocess
import unittest
from cam import ca
from cam import openssl_wrap


logging.basicConfig(level=logging.DEBUG)


class CertStub(object):

    def __init__(self, name, cn, tmpdir):
        self.name = name
        self.cn = cn
        self.alt_names = [cn]
        self.ou = None
        self.days = '365'
        self.public_key_file = os.path.join(tmpdir, '%s.pub' % name)
        self.private_key_file = os.path.join(tmpdir, '%s.priv' % name)

    def get_expiration_date(self):
        return 123456789

    def exists(self):
        return os.path.exists(self.public_key_file)


class CAStub(object):
    def __init__(self, *args, **kwargs):
        self.tmpdir = tempfile.mkdtemp()
        self.ca = ca.CA(self.tmpdir, *args, **kwargs)

    def destroy(self):
        self.ca.close()
        shutil.rmtree(self.tmpdir)


class CATest(unittest.TestCase):

    def setUp(self):
        self.ca_stub = CAStub({'cn': 'test ca', 'org': 'test',
                         'bits': '1024', 'email': 'test@test.com'},
                         password='testpw')
        self.ca = self.ca_stub.ca
        self.tmpdir = self.ca_stub.tmpdir

    def tearDown(self):
        self.ca_stub.destroy()

    def test_create(self):
        self.ca.create()
        self.assertTrue(os.path.exists(os.path.join(self.tmpdir, 'conf/ca.conf')))

    def test_create_cert(self):
        self.ca.create()
        cert = CertStub('test', 'www.test.com', self.tmpdir)
        self.ca.generate(cert)
        self.assertTrue(os.path.exists(cert.public_key_file))
        self.assertTrue(os.path.exists(cert.private_key_file))

    def test_create_cert_with_digest_override(self):
        self.ca.config['signature_algorithm'] = 'md5'
        self.ca.create()
        cert = CertStub('test', 'www.test.com', self.tmpdir)
        self.ca.generate(cert)
        self.assertTrue(os.path.exists(cert.public_key_file))
        self.assertTrue(os.path.exists(cert.private_key_file))
        self.assertTrue(
            'Signature Algorithm: md5WithRSAEncryption' in subprocess.check_output(
            ['openssl', 'x509', '-text', '-noout', '-in', cert.public_key_file]))

    def test_revoke(self):
        self.ca.create()
        cert = CertStub('test', 'www.test.com', self.tmpdir)
        self.ca.generate(cert)
        self.ca.revoke(cert)
        self.assertTrue(os.path.exists(os.path.join(self.tmpdir, 'public/ca.crl')))

    def test_verify(self):
        orig_cert = CertStub('test', 'www.test.com', self.tmpdir)
        self.ca.create()
        self.ca.generate(orig_cert)

        alt_ca = CAStub({'cn': 'test ca', 'org': 'test',
                         'bits': '1024', 'email': 'test@test.com'},
                         password='testpw')
        alt_ca.ca.create()
        alt_cert = CertStub('test', 'www.test.com', alt_ca.tmpdir)
        alt_ca.ca.generate(alt_cert)

        self.assertTrue(self.ca.verify(orig_cert.public_key_file))
        self.assertTrue(alt_ca.ca.verify(alt_cert.public_key_file))
        self.assertFalse(self.ca.verify(alt_cert.public_key_file))
        self.assertFalse(alt_ca.ca.verify(orig_cert.public_key_file))

        alt_ca.destroy()
