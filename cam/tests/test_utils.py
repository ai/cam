import os
import unittest
import tempfile
from cam import utils


class UtilsTest(unittest.TestCase):

    def setUp(self):
        self.old_template_dir = utils._template_dir
        self.tmpdir = tempfile.mkdtemp()
        utils._template_dir = self.tmpdir

    def tearDown(self):
        os.system('rm -fr "%s"' % self.tmpdir)
        utils._template_dir = self.old_template_dir

    def test_render(self):
        tf = os.path.join(utils._template_dir, 'test')
        tfd = open(tf, 'w')
        tfd.write('this is a %(sub)s\n')
        tfd.close()
        of = os.path.join(self.tmpdir, 'test.out')
        utils.render(of, 'test', {'sub':'TEST'})
        self.assertTrue(os.path.exists(of))
        output = open(of, 'r').read()
        self.assertEqual('this is a TEST\n', output)

    def test_parse_bool(self):
        self.assertTrue(utils.parse_bool('y'))
        self.assertTrue(utils.parse_bool('1'))
        self.assertTrue(utils.parse_bool('true'))
        self.assertFalse(utils.parse_bool('false'))
        self.assertFalse(utils.parse_bool('no'))


if __name__ == '__main__':
    unittest.main()
