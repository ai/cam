import os
import tempfile
import time
import shutil
import unittest
from cam import cert


TEST_PEM = '''-----BEGIN CERTIFICATE-----
MIICNDCCAaECEAKtZn5ORf5eV288mBle3cAwDQYJKoZIhvcNAQECBQAwXzELMAkG
A1UEBhMCVVMxIDAeBgNVBAoTF1JTQSBEYXRhIFNlY3VyaXR5LCBJbmMuMS4wLAYD
VQQLEyVTZWN1cmUgU2VydmVyIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTk0
MTEwOTAwMDAwMFoXDTEwMDEwNzIzNTk1OVowXzELMAkGA1UEBhMCVVMxIDAeBgNV
BAoTF1JTQSBEYXRhIFNlY3VyaXR5LCBJbmMuMS4wLAYDVQQLEyVTZWN1cmUgU2Vy
dmVyIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIGbMA0GCSqGSIb3DQEBAQUAA4GJ
ADCBhQJ+AJLOesGugz5aqomDV6wlAXYMra6OLDfO6zV4ZFQD5YRAUcm/jwjiioII
0haGN1XpsSECrXZogZoFokvJSyVmIlZsiAeP94FZbYQHZXATcXY+m3dM41CJVphI
uR2nKRoTLkoRWZweFdVJVCxzOmmCsZc5nG1wZ0jl3S3WyB57AgMBAAEwDQYJKoZI
hvcNAQECBQADfgBl3X7hsuyw4jrg7HFGmhkRuNPHoLQDQCYCPgmc4RKz0Vr2N6W3
YQO2WxZpO8ZECAyIUwxrl0nHPjXcbLm7qt9cuzovk2C2qUtN8iD3zV9/ZHuO3ABc
1/p3yjkWWW8O6tO1g39NTUJWdrTJXwT4OPjr0l91X817/OWOgHz8UA==
-----END CERTIFICATE-----
'''

TEST_SHA1 = '44:63:C5:31:D7:CC:C1:00:67:94:61:2B:B6:56:D3:BF:82:57:84:6F'
TEST_MD5 = '74:7B:82:03:43:F0:00:9E:6B:B3:EC:47:BF:85:A5:93'
TEST_EXPIRY = 1262908799.0


class CAStub(object):

    def __init__(self, basedir):
        self.basedir = basedir


class CertTest(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        os.makedirs(os.path.join(self.tmpdir, 'public', 'certs'))
        self.ca = CAStub(self.tmpdir)
        self.crt_file = os.path.join(self.tmpdir, 'public', 'certs', 'test.pem')
        fd = open(self.crt_file, 'w')
        fd.write(TEST_PEM)
        fd.close()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_get_fingerprint(self):
        crt = cert.Cert(self.ca, 'test', {'cn': 'test.com'})
        md5 = crt.get_fingerprint('md5')
        self.assertEqual(TEST_MD5, md5)
        sha1 = crt.get_fingerprint('sha1')
        self.assertEqual(TEST_SHA1, sha1)

    def test_get_fingerprint_nonexist(self):
        crt = cert.Cert(self.ca, 'test-nonexist', {'cn': 'test.com'})
        result = crt.get_fingerprint('md5')
        self.assertEqual(None, result)

    def test_cn_in_alt_names(self):
        crt = cert.Cert(self.ca, 'test', {'cn': 'test.com',
                                          'alt_names': 'test2.com'})
        self.assertTrue('test.com' in crt.alt_names)

    def test_get_expiration_date(self):
        crt = cert.Cert(self.ca, 'test', {'cn': 'test.com'})
        exp = crt.get_expiration_date()
        self.assertEqual(TEST_EXPIRY, exp)

    def test_get_expiration_date_nonexist(self):
        crt = cert.Cert(self.ca, 'test-nonexist', {'cn': 'test.com'})
        exp = crt.get_expiration_date()
        self.assertEqual(None, exp)

    def test_expired(self):
        crt = cert.Cert(self.ca, 'test', {'cn': 'test.com'})
        exp = crt.get_expiration_date()
        now = time.time()
        is_expired = (exp > now)
        self.assertEqual(is_expired, crt.expired())


if __name__ == '__main__':
    unittest.main()
