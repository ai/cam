import logging
import os
import subprocess

log = logging.getLogger(__name__)


class CommandError(Exception):
    pass


def run(*args, **env_vars):
    return run_with_stdin(None, *args, **env_vars)


def run_with_stdin(stdin, *args, **env_vars):
    cmd = ['/usr/bin/openssl']
    cmd.extend(args)
    env = dict(os.environ)
    env.update(env_vars)
    log.debug('executing "%s"' % ' '.join(cmd))
    popen_kwargs = {'env': env, 'stdout': subprocess.PIPE}
    if stdin is not None:
        popen_kwargs.update({'stdin': subprocess.PIPE})
    pipe = subprocess.Popen(cmd, **popen_kwargs)
    stdout, _ = pipe.communicate(stdin)
    if pipe.returncode != 0:
        raise CommandError('openssl exited with status %d' % (
                pipe.returncode,))
    return stdout


def run_with_config(caroot, config_file, ca_pass=None, *args, **env_vars):
    cmd = args[0]
    args = args[1:]
    caroot = os.path.abspath(caroot)
    env = {'CAROOT': caroot}
    env.update(env_vars)
    run_args = ['-config', config_file, '-batch']
    if ca_pass is not None:
        run_args.extend(['-passin', 'stdin'])
    run_args = run_args + list(args)
    return run_with_stdin(ca_pass, cmd, *run_args, **env)
